package interval.v00;

public class ClosedEndPoint extends EndPoint {
    public ClosedEndPoint(double value, Position position) {
		super(value, position);
	}
	public boolean includes(double value) {
    	return super.includes(value) ||  super.getValue() == value  ;
    }
    @Override
	public boolean includes(EndPoint that) {
		return includes(that.getValue()); 
	}
    
	public boolean isClosed() {
		return true;
	}

}
