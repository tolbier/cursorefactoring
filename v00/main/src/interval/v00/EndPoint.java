package interval.v00;

public class EndPoint {
	public interface Position{
		public boolean includes(double value) ;
		public void setEndPoint(EndPoint endPoint) ;
	}
	public abstract static class PositionImpl implements Position{
		private EndPoint endPoint;
		public abstract boolean includes(double value);
		
		public void setEndPoint(EndPoint endPoint) {
			this.endPoint = endPoint;
		}
		public double getValue() {
			return endPoint.getValue();
		}
		
	}
	public static class Right extends PositionImpl {
		@Override
		public boolean includes(double value) {
	    	return getValue() > value;  	
		}
	}
	public static class Left extends PositionImpl {
		@Override
		public boolean includes(double value) {
	    	return getValue() < value;  	
		}
	}
	private Position position;
	private double value;
	
	public EndPoint(double value,Position position) {
		this.position = position;
		position.setEndPoint(this);
		this.value = value;
	}

	public boolean includes(EndPoint that) {
		if (this.value == that.value) {
			return !(that.isClosed());
		}
		return includes(that.getValue());
	}

	public boolean includes(double value) {
		return this.position.includes(value);
	}

	public void shift(double value) {
		this.value+=value;
	}

	public double getValue() {
		return value;
	}

	public double diff(EndPoint other) {
		return this.value - other.value ;
	}
	
	public boolean isClosed() {
		return false;
	}
}
