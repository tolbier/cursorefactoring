package interval.v00;



public class Interval {

	private EndPoint rightEndPoint;
	private EndPoint leftEndPoint;

	
	public Interval(EndPoint leftEndPoint, EndPoint rightEndPoint) {	
		this.leftEndPoint = leftEndPoint;
		this.rightEndPoint = rightEndPoint;
	}

	public void shift(double value) {
		this.rightEndPoint.shift(value);
		this.leftEndPoint.shift(value);
	}

	public double length() {
		return this.rightEndPoint.diff(this.leftEndPoint);
	}
	
	
	public boolean includes(double value) {
		return ( leftEndPoint.includes(value) && rightEndPoint.includes(value)); 
	}

	public boolean includes(Interval that) {
		return leftEndPoint.includes(that.leftEndPoint) && 
				rightEndPoint.includes(that.rightEndPoint);
	}

	

}
