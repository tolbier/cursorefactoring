package interval.v00;

public class IntervalBuilder {
	private EndPoint leftEndPoint;
	private EndPoint rightEndPoint;
	



	public IntervalBuilder() {

	}

	public IntervalBuilder open(double value) {
		assert rightEndPoint == null;
		if (leftEndPoint == null) {
			leftEndPoint = new EndPoint(value,new EndPoint.Left());
		} else {
			rightEndPoint = new EndPoint(value,new EndPoint.Right());
		}
		return this;
	}

	public IntervalBuilder closed(double value) {
		assert rightEndPoint == null;
		if (leftEndPoint == null) {
			leftEndPoint = new ClosedEndPoint(value,new EndPoint.Left());
		} else {
			rightEndPoint = new ClosedEndPoint(value,new EndPoint.Right());
		}
		return this;
	}

	public Interval build() {
		return new Interval(leftEndPoint, rightEndPoint);
	}
}
